"""
创建，启动一个异步任务并和执行函数进行绑定
"""

# TODO:在celery运行之初，首先加载Django配置环境
# 导入Celery 类,构建构建生成者和消费者模型
from celery import Celery
import os, django

# (1)执行配置文件导包路径
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tensquare.settings.dev')
# (2) 调用Django接口函数手动加载配置，也可省略，django会根据上述执行路径自动执行下面的加载
# django.setup()

# 1。创建异步任务对象
celery_app = Celery('tensquare')

# 2。加载配置文件
# 注意：我们异步程序是在celery_tasks包所在的目录为工作目录运行
celery_app.config_from_object('celery_tasks.config')

# 3。注册异步任务-异步任务在python的包中，参数就是任务函数名
# celery_app.autodiscover_tasks(['celery_tasks.sms'])

celery_app.autodiscover_tasks(['celery_tasks.sms'])
