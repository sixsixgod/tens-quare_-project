from django.core.mail import send_mail
from django.conf import settings
import logging
logger = logging.getLogger('django')

from celery_tasks.main import celery_app


# 定义一个发送邮件的异步函数
@celery_app.task(name='send_verify_email')
def send_verify_email(to_email, verify_url):
    """

    :param to_email: 接收邮件的邮箱列表
    :param verify_url: 用户需要点击的验证链接
    :return: 返回1表示发送成功，0表示发送失败
    """
    # 标题
    subject = "美多商场邮箱验证"

    # 发送内容
    html_message = '<p>用户你好</p>' \
                   '<p>感谢宁使用美多商场</p>' \
                   '<p>你的邮箱地址为: %s 。 请点击此链接激活邮箱 </p>' \
                   '<p><a href = "%s">%s<a></p>' % (to_email, verify_url, verify_url)
    #
    # # 进行发送
    print("第三步: celery异步任务开始执行---邮件已发送")
    result = send_mail(
        subject,
        "",
        settings.EMAIL_FROM,
        [to_email],
        html_message=html_message
    )
    return result
