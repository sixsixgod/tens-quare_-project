"""
定义异步需要执行的函数：
在一个任务包sms里面定义一个固定名称tasks.py，在tasks.py模块
中定义异步任务函数
把任务函数和异步程序进行绑定
"""

from tensquare.celery_tasks.main import celery_app
from ..yuntongxun.ccp_sms import CCP

# celery_app.config_from_object('celery_tasks.config')

# 让 celery_app 自动捕获目标地址下的任务：就是自动获取tasks
# celery_app.autodiscover_tasks(['celery_tasks.sms'])

# 发送短信的异步任务，异步程序装饰器此任务函数
@celery_app.task(name='ccp_send_sms_code')
def ccp_send_sms_code(mobile, sms_code):
    """
    功能：使用yuntongxun发送短信
    参数：mobile手机号，sms_code验证码
    return：云通讯发送短信接口的返回值返回
    """
    result = CCP().send_template_sms(
        mobile,
        [sms_code, 5],
        1)
    return result
