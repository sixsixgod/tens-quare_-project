# 继承重写django的存储后端
from django.core.files.storage import Storage
from django.conf import settings
from fdfs_client.client import Fdfs_client
from rest_framework.exceptions import ValidationError


class FastDFSStorage(Storage):
    def __init__(self):
        self.fdfs_url = settings.FDFS_BASE_URL

    def _open(self, name, mode='rb'):
        pass

    def _save(self, name, content, max_length=None):
        data = content.read()
        fdfs = Fdfs_client(settings.FDFS_FILE)
        res = fdfs.upload_by_buffer(data)
        if res["Status"] != 'Upload successed.':
            raise ValidationError("上传失败")
        return res['Remote file_id']

    # 想要使用save方法被调用,下面这个方法必须返回false
    def exists(self, name):
        return False

    def url(self, name):
        return self.fdfs_url + name
