"""用户模块序列化器"""
from rest_framework import serializers
from .models import User
from django_redis import get_redis_connection
from rest_framework.exceptions import ValidationError
from django.contrib.auth.hashers import make_password


class UserModelSerializer(serializers.ModelSerializer):
    # class Meta:
    #     model = User
    #     fields = "__all__"
    sms_code = serializers.CharField(min_length=6, max_length=6)

    class Meta:
        model = User
        fields = ["username", "password", "mobile", "sms_code"]
        extra_kwargs = {
            "username": {"min_length": 3, "max_length": 20},
            "password": {"write_only": True, "min_length": 3, "max_length": 20},
        }

    def validate(self, attrs):
        """短信校验"""

        sms_code = attrs.pop('sms_code')

        redis_conn = get_redis_connection('verify_code')
        sms_code_server = redis_conn.get('sms_%s' % attrs['mobile'])

        if not sms_code:
            ValidationError('短信验证码过期')

        if sms_code != sms_code_server:
            ValidationError('短信验证码失效')

        # 密码加密
        salt_passwd = make_password(attrs['password'])
        attrs['password'] = salt_passwd
        # attrs['avatar'] =
        return attrs
