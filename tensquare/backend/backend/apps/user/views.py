from rest_framework.viewsets import ModelViewSet
from user.models import *
from user.user_serializer import *


class UserViews(ModelViewSet):
    serializer_class = UserModelSerializer
    queryset = User.objects.all()


