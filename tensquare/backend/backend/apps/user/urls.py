from django.urls import path
from . import views
from rest_framework_jwt.views import obtain_jwt_token
from user.views import *


urlpatterns = [
    # 登录
    path('authorizations/', obtain_jwt_token),
    path('users/', UserViews.as_view({'post': 'create'}))

]