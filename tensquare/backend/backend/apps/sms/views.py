from django.views import View
from django import http
from django_redis import get_redis_connection
import re
import random
# from tensquare.celery_tasks.sms.tasks import ccp_send_sms_code


class SMSCodeView(View):
    def get(self, request, mobile):
        """短信发送"""
        redis_conn = get_redis_connection('verify_code')
        send_flag = redis_conn.get('sms_flag_%s' % mobile)

        if send_flag:
            return http.JsonResponse({
                "success": False,
                'message': '发送短信过于频繁'
            })
        sms_code = "%06d" % random.randrange(0, 999999)

        redis_conn.setex('sms_%s' % mobile, 300, sms_code)
        redis_conn.setex('send_flag_%s' % mobile, 60, 1)

        # ccp_send_sms_code.delay(mobile, sms_code)
        # CCP().send_template_sms(mobile, [sms_code, 5], 1)
        # print("短信验证码是：", sms_code)

        return http.JsonResponse({
            'success': True,
            'sms_code': sms_code,
            'message': "短信发送成功"
        })