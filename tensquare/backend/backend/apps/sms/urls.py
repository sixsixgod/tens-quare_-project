from django.urls import path
from . import views


urlpatterns = [
    path('sms_codes/<mobile:mobile>/', views.SMSCodeView.as_view())
]