from rest_framework import serializers
from .models import Spit


class SpitModelSerializer(serializers.ModelSerializer):
    """吐槽模块序列化器"""
    class Meta:
        model = Spit
        fields = "__all__"

    def create(self, validated_data):
        request = self.context.get('request')           # 获取前端参数
        parent_id = request.data.get('parent')             # 获取父级id
        try:
            user = request.user
        except:
            user = None
        # 判断是否是吐槽别人的吐槽
        if parent_id != None:
            if user and user.is_authenticated:
                validated_data['parent'] = Spit.objects.get(id=parent_id)
                Spit.objects.create(**validated_data)

                # 父级吐槽评论数 +1
                f_id = Spit.objects.get(id=parent_id)
                f_id.comment += 1
                f_id.save()
        else:
            Spit.objects.create(**validated_data)
        return Spit.objects.all()


