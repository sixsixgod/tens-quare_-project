from django.urls import path
from backend.apps.spit.views import *

from django.urls import include
from rest_framework import routers

router = routers.SimpleRouter()

# 吐槽视图集自动路由
router.register(prefix='spit', viewset=SpitView)

urlpatterns = [
    # 收藏和取消收藏
    path('spit/<int:pk>/collect/', CollectView.as_view()),
    path('spit/<int:pk>/updatethumbup/', UpdateTHumBupView.as_view()),

    # 吐槽评论
    path('spit/<int:pk>/children/', ChildrenView.as_view())
]

# 吐槽 get post
urlpatterns += router.urls
print(urlpatterns)