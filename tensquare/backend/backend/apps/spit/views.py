"""吐槽功能模块"""

from rest_framework.generics import ListAPIView, UpdateAPIView, GenericAPIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from spit.spit_serializer import SpitModelSerializer
from spit.models import Spit
from django_redis import get_redis_connection
from django.views import View
from rest_framework import status
from rest_framework.decorators import action


# 分页器
# class Mypage(PageNumberPagination):
#     # page_query_param = 5
#     # page_size_query_param = 'pagesize'
#     max_page_size = 10
#
#     def get_paginated_response(self, data):
#         return Response({
#             'count': self.page.paginator.count,
#             'next': self.get_next_link(),
#             'previous': self.get_previous_link(),
#             'results': data
#         })


# 吐槽功能,get 返回吐槽列表数据 和 post 发布吐槽接口
class SpitView(ModelViewSet):
    pagination_class = None
    serializer_class = SpitModelSerializer
    queryset = Spit.objects.all()

    def get_queryset(self):
        """在列表返回数据时，判断该用户的redis中是否包含收藏和点赞的数"""

        # user_id = self.request.user.id
        # conn = get_redis_connection("collect")
        # print("用户id：", user_id)
        # print("用户在redis中的收藏数据：", conn.scard('collect_%s' % user_id))
        # print("用户在redis中的点赞数据：", conn.scard('hasthumbup_%s' % user_id))
        #
        # if conn.scard('collect_%s' % spit_id) == 0:
        #     Spit.objects.all().update(collected=False)
        # else:
        #     user_coll_dict = conn.smembers('collect_%s' % user_id)
        #     for coll in user_coll_dict:
        #         Spit.objects.filter(id=coll).update(collected=True)
        #
        # if conn.scard('hasthumbup_%s' % bup_id) == 0:
        #     Spit.objects.all().update(hasthumbup=False)
        # else:
        #     user_sme_dict = conn.smembers('hasthumbup_%s' % user_id)
        #     for sme in user_sme_dict:
        #         Spit.objects.filter(id=sme).update(hasthumbup=True)
        conn = get_redis_connection("collect")
        user_id = self.request.user.id
        Spit.objects.all().update(hasthumbup=False, collected=False)  # 初始化所有点赞和收藏状态为 False
        collect_keys_all = conn.keys("collect_*")  # 获取redis中 collect 的所有键
        hasthumbup_keys_all = conn.keys("hasthumbup_*")
        print(collect_keys_all)
        print(hasthumbup_keys_all)

        for key in collect_keys_all:
            num = key.decode().split('_')[1]  # 字符得到数字
            print('collect', num)
            if conn.sismember(key, user_id):  # sismember 检查集合中是否包含 bup_id 这个值
                Spit.objects.filter(id=num).update(collected=True)
        for key in hasthumbup_keys_all:
            num = key.decode().split('_')[1]  # 得到数字
            print('hasthumbup', num)
            if conn.sismember(key, user_id):  # sismember 检查集合中是否包含 bup_id 这个值
                Spit.objects.filter(id=num).update(hasthumbup=True)

        return self.queryset.filter(parent=None).order_by('id')

    # # 收藏或取消收藏
    # @action(methods=['put'], detail=True, url_path='collect')
    # def collect(self, request, *args, **kwargs):
    #     spit_id = self.kwargs.get('pk')
    #     user = request.user
    #     a = user.id
    #     print('用户id', a)
    #
    #     col = Spit.objects.filter(pk=spit_id)
    #     col_obj = col[0]
    #
    #     # 创建redis对象
    #     conn = get_redis_connection("collect")
    #     # print(conn.scard('collect_%s' % a), type(conn.scard('collect_%s' % a)))
    #
    #     # sismember 检查集合中是否包含 bup_id 这个值
    #     if conn.sismember('collect_%s' % user.id, spit_id):
    #         col_obj.collected = False
    #         conn.srem('collect_%s' % user.id, spit_id)
    #         col_obj.save()
    #         return Response({"message": "取消收藏成功", 'success': True})
    #     else:
    #         col_obj.collected = True
    #         conn.sadd('collect_%s' % user.id, spit_id)
    #         col_obj.save()
    #         return Response({"message": "收藏成功", 'success': True})
    #
    # # 点赞和取消点赞
    # @action(methods=['put'], detail=True, url_path='updatethumbup')
    # def updatethumbup(self, request, *args, **kwargs):
    #     bup_id = self.kwargs.get('pk')
    #     user = request.user
    #     col = Spit.objects.get(pk=bup_id)
    #
    #     conn = get_redis_connection("collect")
    #
    #     # sismember 检查集合中是否包含 bup_id 这个值
    #     if conn.sismember('hasthumbup_%s' % user.id, bup_id):
    #         col.hasthumbup = False
    #         col.thumbup -= 1
    #         col.save()
    #         conn.srem('hasthumbup_%s' % user.id, bup_id)
    #         return Response({"message": "取消点赞成功", 'success': True})
    #     else:
    #         col.hasthumbup = True
    #         col.thumbup += 1
    #         col.save()
    #         conn.sadd('hasthumbup_%s' % user.id, bup_id)
    #         return Response({"message": "点赞成功", 'success': True})


# 收藏或取消收藏
class CollectView(UpdateAPIView):
    serializer_class = SpitModelSerializer
    queryset = Spit.objects.all()

    def put(self, request, *args, **kwargs):
        spit_id = self.kwargs.get('pk')
        user = request.user
        a = user.id
        print('用户id', a)

        col = Spit.objects.filter(pk=spit_id)
        col_obj = col[0]

        # 创建redis对象
        conn = get_redis_connection("collect")
        print(conn.scard('collect_%s' % spit_id), type(conn.scard('collect_%s' % spit_id)))

        # sismember 检查集合中是否包含 bup_id 这个值
        if conn.sismember('collect_%s' % spit_id, user.id):
            col_obj.collected = False
            conn.srem('collect_%s' % spit_id, user.id)
            col_obj.save()
            return Response({"message": "取消收藏成功", 'success': True})
        else:
            col_obj.collected = True
            conn.sadd('collect_%s' % spit_id, user.id)
            col_obj.save()
            return Response({"message": "收藏成功", 'success': True})


# 点赞和取消点赞
class UpdateTHumBupView(UpdateAPIView):
    serializer_class = SpitModelSerializer
    queryset = Spit.objects.all()

    def put(self, request, *args, **kwargs):
        bup_id = self.kwargs.get('pk')
        user = request.user
        col = Spit.objects.get(pk=bup_id)

        conn = get_redis_connection("collect")

        # sismember 检查集合中是否包含 bup_id 这个值
        if conn.sismember('hasthumbup_%s' % bup_id, user.id):
            col.hasthumbup = False
            col.thumbup -= 1
            col.save()
            conn.srem('hasthumbup_%s' % bup_id, user.id)
            return Response({"message": "取消点赞成功", 'success': True})
        else:
            col.hasthumbup = True
            col.thumbup += 1
            col.save()
            conn.sadd('hasthumbup_%s' % bup_id, user.id)
            return Response({"message": "点赞成功", 'success': True})


# 获取吐槽评论
class ChildrenView(ListAPIView):
    serializer_class = SpitModelSerializer
    queryset = Spit.objects.all()

    def get_queryset(self):
        id = self.kwargs['pk']
        return self.queryset.filter(parent_id=id)
